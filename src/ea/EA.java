package ea;

/***
 * This is an example of an EA used to solve the problem
 *  A chromosome consists of two arrays - the pacing strategy and the transition strategy
 * This algorithm is only provided as an example of how to use the code and is very simple - it ONLY evolves the transition strategy and simply sticks with the default
 * pacing strategy
 * The default settings in the parameters file make the EA work like a hillclimber:
 * 	the population size is set to 1, and there is no crossover, just mutation
 * The pacing strategy array is never altered in this version- mutation and crossover are only
 * applied to the transition strategy array
 * It uses a simple (and not very helpful) fitness function - if a strategy results in an
 * incomplete race, the fitness is set to 1000, regardless of how much of the race is completed
 * If the race is completed, the fitness is equal to the time taken
 * The idea is to minimise the fitness value
 */


import java.util.ArrayList;
import teamPursuit.TeamPursuit;
import teamPursuit.WomensTeamPursuit;

//public class EA implements Runnable{
public class EA {
	// create a new team with the default settings
	public static TeamPursuit teamPursuit = new WomensTeamPursuit(); 
//	int run = 0;


	private ArrayList<Individual> population = new ArrayList<Individual>();
	private ArrayList<Individual> fullPopulation = population;
	
	private int iteration = 0;
	
	public EA() {
		ArrayList <Individual> multiRunStats = new ArrayList <Individual>();
	}

	public static void main(String[] args) {
		
		for (int y = 1; y <= Parameters.numberOfRuns; y++){
			EA ea = new EA();
			ea.run(y);
		}
	}

	public void run(int run) {
		//Run EVOLUTIONARY ALGORITHM
		initialisePopulation();	
		System.out.println("finished init pop");
		iteration = 0;
		while(iteration < Parameters.maxIterations){
			iteration++;
			
			//SELECTION
			Individual parent1 = tournamentSelection();
			Individual parent2 = tournamentSelection();
			
			//CROSSOVER
			Individual child = uniformCrossover(parent1, parent2);
			
			//MUTATION
			child = swapMutate(child);
			child.evaluate(teamPursuit);
			
			//REPLACEMENT
			replace(child);
			
			//INFUSION
			infusePopulation();
			
			//PRINT
			if(Parameters.numberOfRuns == 1){
				printStats();
			}
		}						
		Individual best = getBest(population);
//		best.print();
		this.multiRunStats.add(best);
		this.multiRunStats.add(getWorst(population));
		
		//if last multi run, print stats
		if (Parameters.numberOfRuns != 1 && Parameters.numberOfRuns == run)
		{
			printMultiRunStats();
		}
	}
	
	public void runIslandEA() {
		// Run MULTI MODEL ISLAND EVOLUTIONARY ALGORITHM
		initialiseIslandPopulation();
		System.out.println("finished init pop");
		iteration = 0;
		Parameters.popSize = Parameters.sizeOfIsland;
		int lastMigration = 0;
		while (iteration < Parameters.maxIterations) {
			iteration++;
			for (int i = 0; i < Parameters.numberOfIslands; i++) {
				if (i == 0) {
					population = new ArrayList<Individual>(
							fullPopulation.subList(0,
									Parameters.sizeOfIsland));
				} else {
					population = new ArrayList<Individual>(
							fullPopulation.subList(i * Parameters.sizeOfIsland, (i + 1) * Parameters.sizeOfIsland));
				}
				// SELECTION
				Individual parent1 = tournamentSelection();
				Individual parent2 = tournamentSelection();

				// CROSSOVER
				Individual child = uniformCrossover(parent1, parent2);

				// MUTATION
				child = swapMutate(child);
				child.evaluate(teamPursuit);

				// REPLACEMENT
				replace(child);

				// PRINT
				if (iteration % 100 == 0){
					printStats();
				}
				
				infusePopulation();
				
				// replace fullPopulations population segment with new
				// population
				if (i == 0) {
					//population = new ArrayList<Individual>(fullPopulation.subList(0,Parameters.numberOfIslands));
					for (int x = 0; x < Parameters.sizeOfIsland; x++) {
						fullPopulation.set(x, population.get(x));
					}
				} else {
					//fullPopulation.subList(i * Parameters.sizeOfIsland + 1, (i + 1) * Parameters.sizeOfIsland));
					for (int x = 0; x < Parameters.sizeOfIsland; x++) {
						fullPopulation.set((i*Parameters.sizeOfIsland) + x, population.get(x));
					}
				}
			}
			if (iteration == Parameters.migrationInterval
					&& lastMigration != iteration) {
				lastMigration = iteration;
				islandMigrations();
			}
		}
		Individual best = getBest(fullPopulation);
		best.print();

	}

	private void printStats() {		
		System.out.println("" + iteration + "\t" + getBest(population) + "\t" + getWorst(population));		
	}
	
	private void printMultiRunStats(){
		for (int i = 0; i < multiRunStats.size(); i+=2)
		{
			System.out.println("RUN " + (i+1));
			multiRunStats.get(i).print();
			multiRunStats.get(i+1).print();
			System.out.println("\n");
			
		}
	}


	private void replace(Individual child) {
		//If new child is better than worst in population, replace accordingly
		Individual worst = getWorst(population);
		if(child.getFitness() < worst.getFitness()){
			int idx = population.indexOf(worst);
			population.set(idx, child);
		}
	}
	
	private void tournamentReplacement(Individual child){
		// Tournament replacement)
			int worstIdx = 0;
			Individual worst = null;
			for (int i = 0; i < 5; i++) {
				int idx = Parameters.rnd.nextInt(Parameters.popSize);
				Individual x = population.get(idx);
				if (worst == null || x.getFitness() > population.get(worstIdx).getFitness()) {
					worst = x;
					worstIdx = idx;
				}
			}
			population.set(worstIdx, child);
	}
	
	private void islandMigrations(){
		ArrayList <String> bestIndex = new ArrayList <String> ();
		ArrayList <Individual> best = new ArrayList <Individual> ();
		//Best chromosome's travel to next island
		for (int i = 0; i < Parameters.numberOfIslands; i++){
			if (i == 0){
				population = new ArrayList<Individual>(fullPopulation.subList(0, Parameters.sizeOfIsland));
			}
			else{
				population = new ArrayList<Individual>(fullPopulation.subList(i * Parameters.sizeOfIsland + 1, (i + 1) * Parameters.sizeOfIsland));
			}
			bestIndex.add(Integer.toString(getBestIndex(population)));
			best.add(getBest(population));
		}
		for (int i = 0; i < Parameters.numberOfIslands; i++){
			if (i == 0){
				population = new ArrayList<Individual>(fullPopulation.subList(0, Parameters.sizeOfIsland));
			}
			else{
				population = new ArrayList<Individual>(fullPopulation.subList(i * Parameters.sizeOfIsland + 1, (i + 1) * Parameters.sizeOfIsland));
			}
			if (i == 0){
				population.set(Integer.parseInt(bestIndex.get(i)), best.get(Parameters.numberOfIslands-1));
			}
			else{
				population.set(Integer.parseInt(bestIndex.get(i)), best.get(i-1));
			}
		}
		
	}

	//Method to return random number that is different from the existing number
	public int secondSeparateRandom(int existingIndex, int size){
		int secondRandom;
			//if they are the same, modify the second accordingly depending on its position
			if (existingIndex < size / 2){
				secondRandom = existingIndex + 1 + Parameters.rnd.nextInt(size / 2);
			}
			else {
				secondRandom = existingIndex - Parameters.rnd.nextInt(size / 2);
			}
		return secondRandom;
	}
	

	private Individual mutate(Individual child) {
		if(Parameters.rnd.nextDouble() > Parameters.mutationProbability){
			return child;
		}
		// choose how many elements to alter
		int mutationRate = 1 + Parameters.rnd.nextInt(Parameters.mutationRateMax);
		// mutate the transition strategy
			//mutate the transition strategy by flipping boolean value
			for(int i = 0; i < mutationRate; i++){
				int index = Parameters.rnd.nextInt(child.transitionStrategy.length);
				child.transitionStrategy[index] = !child.transitionStrategy[index];
			}
		return child;
	}
	
	//SWAP MUTATION
	private Individual swapMutate(Individual child) {
		if(Parameters.rnd.nextDouble() > Parameters.mutationProbability){
			return child;
		}
		
		int pos1 = new java.util.Random().nextInt(child.pacingStrategy.length);
		int pos2 = secondSeparateRandom(pos1, child.pacingStrategy.length);
		
		Individual temp = child;
		child.pacingStrategy[pos1] = child.pacingStrategy[pos2];
		child.pacingStrategy[pos2] = temp.pacingStrategy[pos1];
		
		return child;
	}

	//ONE POINT CROSSOVER - exclusively transition 
	private Individual defaultCrossover(Individual parent1, Individual parent2) {
		if(Parameters.rnd.nextDouble() > Parameters.crossoverProbability){
			return parent1;
		}
		Individual child = new Individual();
		int crossoverPoint = Parameters.rnd.nextInt(parent1.transitionStrategy.length);
		
		// just copy the pacing strategy from p1 - not evolving in this version
		for(int i = 0; i < parent1.pacingStrategy.length; i++){			
			child.pacingStrategy[i] = parent1.pacingStrategy[i];
		}
		
		for(int i = 0; i < crossoverPoint; i++){
			child.transitionStrategy[i] = parent1.transitionStrategy[i];
		}
		for(int i = crossoverPoint; i < parent2.transitionStrategy.length; i++){
			child.transitionStrategy[i] = parent2.transitionStrategy[i];
		}
		return child;
	}
	
	//TWO POINT CROSSOVER - exclusively transition
	private Individual twoPointCrossover(Individual parent1, Individual parent2) {
		if(Parameters.rnd.nextDouble() > Parameters.crossoverProbability){
			return parent1;
		}
		Individual child = new Individual();
		int firstCrossoverPoint = Parameters.rnd.nextInt(parent1.transitionStrategy.length);
		int secondCrossoverPoint = Parameters.rnd.nextInt(parent1.transitionStrategy.length);
		
		// ensure first and second point are different
		if (firstCrossoverPoint == secondCrossoverPoint){
			//if they are the same, modify the second accordingly depending on its position
			if (firstCrossoverPoint >= parent1.transitionStrategy.length){
				secondCrossoverPoint = firstCrossoverPoint + 1 + Parameters.rnd.nextInt(parent1.transitionStrategy.length / 2);
			}
			else {
				secondCrossoverPoint = firstCrossoverPoint + 1 - Parameters.rnd.nextInt(parent1.transitionStrategy.length-1 / 2);
			}
		}

		// just copy the pacing strategy from p1 - not evolving in this version
		for(int i = 0; i < parent1.pacingStrategy.length; i++){			
			child.pacingStrategy[i] = parent1.pacingStrategy[i];
		}
		
		for(int i = 0; i < parent1.transitionStrategy.length; i++){
			if (i < firstCrossoverPoint || i > secondCrossoverPoint){
				child.transitionStrategy[i] = parent1.transitionStrategy[i];
			}
			else{
				child.transitionStrategy[i] = parent2.transitionStrategy[i];
			}
			
		}
		return child;
	}

	//UNIFORM CROSSOVER
	private Individual uniformCrossover(Individual parent1, Individual parent2) {
		if(Parameters.rnd.nextDouble() > Parameters.crossoverProbability){
			return parent1;
		}
		Individual child = new Individual();
		
		//Randomly choose to crossover either the pacing or transition strategy
		if (Parameters.rnd.nextInt(2) == 1){
			for(int i = 0; i < parent1.pacingStrategy.length; i++){		
				if (Parameters.rnd.nextInt(2) == 1){
				child.pacingStrategy[i] = parent1.pacingStrategy[i];
				}
				else{
					child.pacingStrategy[i] = parent2.pacingStrategy[i];
				}
			}
			for(int i = 0; i < parent1.transitionStrategy.length; i++){			
				child.transitionStrategy[i] = parent1.transitionStrategy[i];
			}
		}
		else{
			for(int i = 0; i < parent1.transitionStrategy.length; i++){
				if (Parameters.rnd.nextInt(2) == 1){
					child.transitionStrategy[i] = parent1.transitionStrategy[i];
				}
				else{
					child.transitionStrategy[i] = parent2.transitionStrategy[i];
				}

				
			}
			for(int i = 0; i < parent1.pacingStrategy.length; i++){			
				child.pacingStrategy[i] = parent1.pacingStrategy[i];
			}
		}	
		return child;
	}

	/**
	 * Returns a COPY of the individual selected using tournament selection
	 * @return
	 */
	private Individual tournamentSelection() {
		ArrayList<Individual> candidates = new ArrayList<Individual>();
		for(int i = 0; i < Parameters.tournamentSize; i++){
			candidates.add(population.get(Parameters.rnd.nextInt(population.size())));
		}
		return getBest(candidates).copy();
	}
	
	private Individual rouletteSelection() {		
		//Roulette Selection
		Individual picked = null;
		double totalFitness = 0;
		for(Individual individual : population) {
			totalFitness += (individual.getFitness());
		}	
		int r = new java.util.Random().nextInt((int)totalFitness);
		totalFitness = 0;
		for(Individual individual : population) {
			totalFitness += (individual.getFitness());
			if(r < totalFitness){
				picked=individual;
				break;
			}
		}	
		return picked.copy();
	}

	
	private void infusePopulation() {
		int rand = Parameters.rnd.nextInt(80);
		if (rand == 1) {
			for (int i = 0; i < Parameters.sizeOfIsland; i++) {
				Individual random = new Individual();
				random.initialise_within_range();
				random.evaluate(teamPursuit);

				Individual r = population.get(i);
				if (r != getBest(population)) {
					int idx = population.indexOf(r);
					population.set(idx, random);
				}
			}
		}
	}

	private Individual getBest(ArrayList<Individual> aPopulation) {
		double bestFitness = Double.MAX_VALUE;
		Individual best = null;
		for(Individual individual : aPopulation){
			if(individual.getFitness() < bestFitness || best == null){
				best = individual;
				bestFitness = best.getFitness();
			}
		}
		return best;
	}
	
	private int getBestIndex(ArrayList<Individual> aPopulation) {
		double bestFitness = Double.MAX_VALUE;
		int best = -1;
		for(int i = 0; i < aPopulation.size(); i++){
			if(aPopulation.get(i).getFitness() < bestFitness || best == -1){
				best = i;
			}
		}
		return best;
	}

	private Individual getWorst(ArrayList<Individual> aPopulation) {
		double worstFitness = 0;
		Individual worst = null;
		for(Individual individual : population){
			if(individual.getFitness() > worstFitness || worst == null){
				worst = individual;
				worstFitness = worst.getFitness();
			}
		}
		return worst;
	}
	
	private void printPopulation() {
		for(Individual individual : population){
			System.out.println(individual);
		}
	}

	private void initialisePopulation() {
		while(population.size() < Parameters.popSize){
			Individual individual = new Individual();
			individual.initialise_within_range();			
			individual.evaluate(teamPursuit);
			population.add(individual);
							
		}		
	}	
	
	private void initialiseIslandPopulation() {
		while(population.size() < Parameters.numberOfIslands * Parameters.sizeOfIsland){
			Individual individual = new Individual();
			individual.initialise_within_range();
			individual.evaluate(teamPursuit);
			population.add(individual);
							
		}		
	}
}
